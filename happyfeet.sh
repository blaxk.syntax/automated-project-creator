#!/bin/bash

gitrepo=""
language=""
project=""
domain=""
dir = ${pwd}

while getopts ':g:l:p:d:' OPTION; do
    case "$OPTION" in 
        g)
            gitrepo=$OPTARG
            ;;
        l)
            language=$OPTARG
            ;;
        p)
            project=$OPTARG
            ;;
        d)
            domain=$OPTARG
            ;;
        ?)
            echo "script usage: $(basename\$0)
            [-l]
            [-n]"
            exit 1
            ;;
    esac
done

if [ -z "$gitrepo" ];then
    echo "ok"
    if [ -z "$language" ];then
        echo ok ok
    else
        if [ "$language"=="java" ];then
            mvn archetype:generate -DgroupId=$domain -DartifactId=$project -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
        else if [ "$language"=="angular" ];then
            ng new $project --directory .
        fi
    fi
else
    echo "hello"
    git clone $gitrepo
    cd $gitrepo
    if [ -z "$language" ];then
        echo ok ok
    else
        if [ "$language"=="java" ];then
            mvn archetype:generate -DgroupId=$domain -DartifactId=$project -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
        else if [ "$language"=="angular" ];then
            ng new $project --directory .
        fi
    fi
fi